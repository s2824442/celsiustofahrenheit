package nl.utwente.di.temperatureConverter;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class Converter extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius to Fahrenheit";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Temperature in Celsius: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                   Double.toString(Double.parseDouble(request.getParameter("Celsius")) * 1.8 + 32) +
                "</BODY></HTML>");
  }
  

}
